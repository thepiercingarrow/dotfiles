; Key bindings
(global-unset-key "\C-z")
(global-set-key "\C-z" 'advertised-undo)

; Functions
(defun undo-all ()
  "Undo all edits."
  (interactive)
  (when (listp pending-undo-list)
    (undo))
  (while (listp pending-undo-list)
    (undo-more 1))
  (message "Buffer was completely undone"))
