## Copyright (C) 2016 Mark Wright <thepiercingarrow@gmail.com>
## Liscened under WTFPL

if [[ $OSTYPE == darwin* ]]; then
    OS=OSX
else
    OS=LINUX
fi

# Setting environment variables

## editor
export VISUAL="emacs"
export EDITOR="emacs"

## apps
if [[ $OS == OSX ]]; then
    PATH="$PATH:~/Applications/"
fi

# Making aliases

## package manager
case $OS in
OSX) alias b="brew"
alias bi="brew install"
alias prm="brew uninstall"
alias bgrep="brew search"
alias bls="brew list"
alias cask="brew cask"
alias c="brew cask"
alias cls="brew cask list"
alias ci="brew cask install" ;;
LINUX) alias p="sudo pacman"
alias rmp="sudo pacman -Rs"
alias psearch="pacman -Ss"
alias y="yaourt"
alias pa="pacaur" ;;
esac

## programs
alias pm="palemoon"
alias tw="teeworlds"
alias hclient="teeworlds-hclient"
alias e="emacs"
alias an="asciinema"
alias screenfetch="screenfetch -t"

## update files
if [[ $OS == LINUX ]]; then
alias mu="sudo reflector --verbose --country 'United States' -l 200 -p http --sort rate --save /etc/pacman.d/mirrorlist"
alias up="sudo pacman -Syu"
fi

## prevent accidents
alias rm="rm -iv"
alias mv="mv -iv"
alias cp="cp -iv"
alias rmr="rm -rf"
alias rmedir="rmdir"

## git
alias clone="git clone"
alias add="git add -A"
alias commit="git commit -a"
alias push="git push origin master"
alias pull="git pull"
alias PUSH="add && commit && push"

## interface
case $OS in
OSX) alias ls="ls -G" ;;
LINUX) alias ls="ls --color" ;;
esac
alias curl="curl -#"

# Defining functions

## curling dotfiles
curldots() {
if curl --output /dev/null --silent --head --fail 'http://ix.io/rx1'; then
    curl -# http://ix.io/rx1 | sh
else
    curl -# http://pastebin.com/raw/ew7bfwbr | sh
fi
source ~/.bashrc
}

## find and replace
frp() {
    if [ $1 = '-h' ]; then
        echo -e 'usage:\n\tfrp file|directory search_text replace_text'
        echo -e 'example:\n\tfrp src/ awesome superb'
    elif [ -d $1 ]; then
        grep -rl $2 $1 | xargs sed -i .frp.bak "s/$2/$3/g"
    elif [ -f $1 ]; then
        sed -i .frp.bak "s/$2/$3/g" $1
    else
        echo "$1: file/directory not found"
    fi
}

## ix.io client
ix() {
    local opts
    local OPTIND
    [ -f "$HOME/.netrc" ] && opts='-n'
    while getopts ":hd:i:n:" x; do
        case $x in
            h) echo "ix [-d ID] [-i ID] [-n N] [opts]"; return;;
            d) $echo curl $opts -X DELETE ix.io/$OPTARG; return;;
            i) opts="$opts -X PUT"; local id="$OPTARG";;
            n) opts="$opts -F read:1=$OPTARG";;
        esac
    done
    shift $(($OPTIND - 1))
    [ -t 0 ] && {
        local filename="$1"
        shift
        [ "$filename" ] && {
            curl $opts -F f:1=@"$filename" $* ix.io/$id
            return
        }
        echo "^C to cancel, ^D to send."
    }
    curl $opts -F f:1='<-' $* ix.io/$id
}

## git squashing
squash() {
read -n1 -p "Squash last $1 commits for $(pwd)? [Y/n] " squash_confirm
case $squash_confirm in
    [yY]*) git reset --soft HEAD~"$1" && git commit ;;
    *) echo 'Aborted' ;;
esac
}

#run screenfetch
clear
screenfetch
